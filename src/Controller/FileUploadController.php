<?php

namespace Drupal\file_upload\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\media_entity\Entity\Media;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FileUploadController extends ControllerBase {

  function handle(Request $request){

    $destination = 'private://uploads';
    file_prepare_directory($destination, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    $file = file_save_upload('file', array(), $destination, 0);
    return JsonResponse::create(['fid' => $file->id(), 'uuid' => $file->uuid()]);

  }

}
